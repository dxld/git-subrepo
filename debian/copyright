Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: git-subrepo
Upstream-Contact: Ingy döt Net <ingy@ingy.net>
Source: https://github.com/ingydotnet/git-subrepo/

Files: *
Copyright:
 2013-2020, Ingy döt Net
 2006-2008, Ian Macdonald <ian@caliban.org>
 Magnus Carlsson <grimmymail@gmail.com>
 Austin Morgan <admorgan@morgancomputers.net>
License: MIT
 The MIT License (MIT)
 .
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.

Files: share/git-completion.bash
Copyright:
 2006,2007 Shawn O. Pearce <spearce@spearce.org>
 2006-2008 Ian Macdonald <ian@caliban.org>
 2009-2010 Bash Completion Maintainers <bash-completion-devel@lists.alioth.debian.org>
License: GPL-2+ and GPL-2

License: GPL-2
 Distributed under the GNU General Public License, version 2.0.
 .
 On Debian systems, the full text of the GNU General Public
 License version 2 can be found in the file
 `/usr/share/common-licenses/GPL-2'.

License: GPL-2+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software Foundation,
 Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
 .
 On Debian systems, the full text of the GNU General Public
 License version 2 can be found in the file
 `/usr/share/common-licenses/GPL-2'.

Files: debian/*
Copyright:
  2024-2025 Samo Pogačnik <samo_pogacnik@t-2.net>
  2020 Daniel Gröber <dxld@darkboxed.org>
License: GPL-2+
